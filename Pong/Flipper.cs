﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Pong {
    class Flipper {
        Color color = Color.White;
        public Texture2D tex;
        public Vector2 vec;

        public Flipper() {
            //sprite.Begin();
        }

        public void Initialize(Texture2D texture, Vector2 position) {
            tex = texture;
            vec = position;
        }

        public void Draw(SpriteBatch batch) {
            batch.Draw(tex, vec, color);
        }
    }
}
