﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Mario_Clone {
    public interface IGameObject {
        Vector2 Position { get; set; }
        Rectangle Bounds { get; set; }
        
    }
}
