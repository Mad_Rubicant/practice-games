﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Shooter {
    public static class Logging {
        static FileStream output;
        static StreamWriter writer;
        static Logging() {
             output = File.Open("Logs/mainlog.txt", FileMode.Truncate);
             writer = new StreamWriter(output);
        }
        public static void Log(string message) {
            writer.WriteLine(message);
        }
    }
}
