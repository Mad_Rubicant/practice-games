﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Mario_Clone {
    class Map {
        Texture2D tileset;
        Dictionary<Point, Tile> tiles;
        int texturesize = 32;
        public Map(Texture2D set) {
            tileset = set;
            tiles = new Dictionary<Point, Tile>();
            
        }

        public bool Contains(Tile t) {
            Point p = t.Position;
            p.X -= p.X % texturesize;
            p.Y -= p.Y % texturesize;
            return tiles.ContainsKey(p);
        }

        public void Add(Tile t) {
            if (!Contains(t)) {
                Point p = t.Position;
                p.X -= p.X % texturesize;
                p.Y -= p.Y % texturesize;
                t.Position = p;
                Console.WriteLine(p.ToString());
                tiles.Add(p, t);
            }
        }

        public void Update(Tile t) {
            if (Contains(t)) {
                Point p = t.Position;
                p.X -= p.X % texturesize;
                p.Y -= p.Y % texturesize;
                tiles[p] = t;
            }
            else {
                Add(t);
            }
        }

        // Gets the tile at pos, or the one whose rectangle contains pos
        // If no such tile exists, return a blank tile at pos
        public Tile Get(Point position) {
            // First, truncate pos to a multiple of texturesize
            position.X -= position.X % texturesize;
            position.Y -= position.Y % texturesize;
            if (tiles.ContainsKey(position)) {
                return tiles[position];
            }
            else {
                return new Tile(0, position);
            }
        }

        public Tile Get(Vector2 position) {
            Point pos = position.ToPoint();
            pos.X -= pos.X % texturesize;
            pos.Y -= pos.Y % texturesize;
            if (tiles.ContainsKey(pos)) {
                return tiles[pos];
            }
            else {
                return new Tile(0, pos);
            }
        }
        // screenPos is the vector representing the camera, which is a vector pointing to the top left of the viewable area
        public void Draw(SpriteBatch batch, Vector2 screenPos) {
            Point camera = screenPos.ToPoint();
            int topBound = batch.GraphicsDevice.Viewport.TitleSafeArea.Y + camera.Y;
            int leftBound = batch.GraphicsDevice.Viewport.TitleSafeArea.X + camera.X;
            int botBound = batch.GraphicsDevice.Viewport.TitleSafeArea.Height + camera.Y;
            int rightBound = batch.GraphicsDevice.Viewport.TitleSafeArea.Width + camera.X;

            foreach (var v in tiles) {
                Tile t = v.Value;
                // If any part of the tile is within the drawing bounds, draw it
                if (t.Bounds.X < rightBound || t.Bounds.X > leftBound || t.Bounds.Y < botBound || t.Bounds.Y > topBound)
                    batch.Draw(tileset, new Rectangle(t.Position.X - camera.X, t.Position.Y - camera.Y, Tile.textureSize, Tile.textureSize), t.Bounds, Color.White);
            }
        }

        // Deserialize a map. 
        public void FromFile(string filename) {
            using (BinaryReader file = new BinaryReader(File.Open(filename, FileMode.Open))) {
                tiles.Clear();
                while (file.BaseStream.Position != file.BaseStream.Length) {
                    short type = file.ReadInt16();
                    float x = file.ReadInt32();
                    float y = file.ReadInt32();
                    Tile t = new Tile(type, new Vector2(x, y));
                    tiles.Add(t.Position, t);
                }
            }
        }

        // Serialize a map. Representation is short-float-float
        // Good luck opening in a text editor
        public void ToFile(string filename) {
            FileInfo f = new FileInfo(filename);
            if (f.Exists) {
                FileInfo backup = new FileInfo(filename + ".backup");
                if (backup.Exists)
                    backup.Delete();
                f.CopyTo(filename + ".backup");
            }
            using (BinaryWriter file = new BinaryWriter(File.Open(filename, FileMode.Create))) {
                foreach (var v in tiles) {
                    Tile t = v.Value;
                    file.Write(t.tileType);
                    file.Write(t.Position.X);
                    file.Write(t.Position.Y);
                }
            }
        }

        // This dumps the data of a map to a file, in a human-readable format. 
        // For debugging only
        public void DumpMapData() {
            using (StreamWriter file = new StreamWriter(File.Open("map.dump", FileMode.Create))) {
                foreach (var v in tiles) {
                    Tile t = v.Value;
                    file.WriteLine("ID: {0}, Position: {1}, {2}", t.tileType, t.Position.X, t.Position.Y);
                }
            }
        }
    }
}
