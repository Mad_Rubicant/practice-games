﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Mario_Clone {
    class Animation {
        Texture2D spriteStrip;
        float scale;
        int elapsedTime;
        int frameTime;
        int frameCount;
        int currentFrame;
        Color color;
        Rectangle sourceRect = new Rectangle();
        Rectangle destRect = new Rectangle();
        public Rectangle Frame;
        public bool Active;
        public bool Looping;
        public Vector2 Position;

        // Parameters
        // texture: The texture sheet that the animation reads from
        // position: The location on the screen where the texture draws
        // Frame: The size of the animation's textures on the strip
        // frame_count: The number of frames on the animation
        // frame_time: The time that elapses per frame
        // col: Color filter of the animation
        // scale: How much the object is scaled up. 1.0 is original size
        public void Initialize(Texture2D texture, Vector2 position, Rectangle frame, int frame_count, int frame_time,
        Color col, float scale, bool Looping) {
            this.Position = position;
            this.Frame = frame;
            this.frameCount = frame_count;
            this.frameTime = frame_time;
            this.color = col;
            this.scale = scale;
            this.Looping = Looping;
            this.spriteStrip = texture;

            elapsedTime = 0;
            currentFrame = 0;

            Active = true;
        }

        public void Update(GameTime gameTime) {
            if (Active == false) 
                return;

            elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime > frameTime) {
                currentFrame++;

                if (currentFrame == frameCount) {
                    currentFrame = 0;
                    if (Looping == false)
                        Active = false;
                }
                elapsedTime = 0;
            }

            sourceRect = new Rectangle(currentFrame * Frame.Width, 0, Frame.Width, Frame.Height);
            destRect = new Rectangle(((int)Position.X  - (int)(Frame.Width * scale) / 2), (int)(Position.Y - (Frame.Height * scale) / 2),
                (int)(Frame.Width * scale), (int)(Frame.Height * scale));
        }
        public void Draw(SpriteBatch batch) {
            if (Active) {
                batch.Draw(spriteStrip, destRect, sourceRect, color);
                //Logging.Log(destRect.ToString());
                //Logging.Log(spriteStrip.Name);
            }
        }

    }
}
