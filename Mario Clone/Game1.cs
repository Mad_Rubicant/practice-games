﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using System;
using System.Collections.Generic;

namespace Mario_Clone {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Mario_Clone : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        int textureSize = 32;
        Texture2D tilesheet;
        Player player;
        // Camera points to the top left of the viewable area
        Vector2 Camera;
        
        Map map;
        public Mario_Clone()
            : base() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            // TODO: Add your initialization logic here
            Tile.textureSize = 32;
            Camera = new Vector2();
            base.Initialize();
            
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            tilesheet = Content.Load<Texture2D>("art/tileset bosque");
            map = new Map(tilesheet);
            map.FromFile("test.map");
            player = new Player();
            Animation playerAnimation = new Animation();
            playerAnimation.Initialize(tilesheet, new Vector2(100, 100), new Rectangle(128, 0, 32, 32), 1, 1, Color.White, 1.5f, true);
            player.Initialize(playerAnimation);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            player.Update(gameTime);
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            
            GraphicsDevice.Clear(Color.CornflowerBlue);
            // TODO: Add your drawing code here
            spriteBatch.Begin();

            map.Draw(spriteBatch, Camera);
            player.Draw(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void Collisions() {
            // Check everything against everything else. Any collisions found are packed into a tuple

            List<Tuple<IGameObject>> detectedCollisions = new List<Tuple<IGameObject>>();
        }
    }
}
