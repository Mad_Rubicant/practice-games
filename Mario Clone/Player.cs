﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Mario_Clone {
    class Player : IGameObject {
        Texture2D tex;
        Vector2 position;
        Vector2 velocity;
        bool Alive;
        int HP;
        int Lives;
        Animation playerAnimation;

        public Rectangle Bounds {
            get {
                return new Rectangle((int)position.X, (int)position.Y, playerAnimation.Frame.Width, playerAnimation.Frame.Height);
            }
        }
        public Player() {
            Alive = true;
            HP = 1;
            Lives = 5;
            position = new Vector2();
            velocity = new Vector2();            
        }

        public void Initialize(Animation animation) {
            playerAnimation = animation;
        }

        public void Update(GameTime gameTime) {
            velocity.Y += 1;
            position += velocity;
            

            playerAnimation.Position = position;
            playerAnimation.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch) {
            playerAnimation.Draw(spriteBatch);
        }

    }
}
