﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
namespace Mario_Clone {

    struct PhysicsComponent {
        public Vector2 velocity;
        public Vector2 Position;
    }

    struct GraphicsComponent {
        public Texture2D texture;
        public Rectangle texBounds;
    }

    struct InputComponent {
        public KeyboardState state;
    }
}
