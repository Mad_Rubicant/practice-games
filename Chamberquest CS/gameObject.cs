﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Chamberquest_CS {
    class gameObject {
        Animation animation;
        Vector2 Position;
        public virtual void Initialize() {
            ;
        }

        public void Update(GameTime time) {
            animation.Update(time); 
        }

        public void Draw(SpriteBatch batch) {
            animation.Draw(batch);
        }
    }
}
