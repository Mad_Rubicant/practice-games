﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Chamberquest_CS {
    // This class is to be used for static tiles. If it's animated, use the derived class animatedTile
    // Also, this is only for the graphical representation of tiles
    class Tile {
        Texture2D tileSheet;
        Rectangle tileBounds;
        Borders borders;

        public void Initialize(Texture2D tilesheet, Rectangle bounds, Borders borders) {
            tileSheet = tilesheet;
            tileBounds = bounds;
            this.borders = borders;
        }

        public void Update() {

        }

        public void Draw() {
            
        }

        // This struct determines rendering rules. Start at the top-left, and for each tile, pick something that meets its border
        // requirements to draw. 
        }

        internal struct Borders {
            Types upType;
            Types rightType;
            Types downType;
            Types leftType;
            public enum Types {
                Null, Floor, Wall, Stair
            }
    }
}
