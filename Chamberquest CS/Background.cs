﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Chamberquest_CS {
    class Background {
        Texture2D texture;
        // Positions of the layer arrays and each instance among them
        Vector2[][] positions;
        Rectangle layerSize;
        Rectangle screenSize;
        int[] speed;
        int num_layers;
        bool moving = true;
        bool moveLeft = true;

        public void Initialize(Texture2D image, Rectangle layerSize, Rectangle screen, int num_layers) {
            texture = image;
            this.speed = new int[num_layers];
            this.layerSize = layerSize;
            screenSize = screen;
            // Current case, 4 by screensize/width
            positions = new Vector2[num_layers][];
            speed = new int[num_layers];
            this.num_layers = num_layers;
            for (int i = 0; i < num_layers; i++) {
                positions[i] = new Vector2[(screen.Width / screen.Width) + 2];
                speed[i] = i + 1;
            }
            for (int i = 0; i < num_layers; i++) {
                for (int j = 0; j < positions[i].Length; j++) {
                    positions[i][j] = new Vector2((j - 1) * screenSize.Width, 0);
                }
            }
        }

        public void Move(bool left) {
            moving = true;
            if (left == true) {
                moveLeft = true;
            }
            else {
                moveLeft = false;
            }

        }
        public void Update(GameTime gameTime) {
            if (moving == true) {
                for (int i = 0; i < num_layers; i++) {
                    for (int j = 0; j < positions[i].Length; j++) {
                        // If moving left, add speed, check for tiles all the way off to the right
                        // If there's any, move them all the way to the left
                        if (moveLeft == true) {
                            positions[i][j].X += speed[i];
                            if (positions[i][j].X >= screenSize.Width) {
                                positions[i][j].X = -screenSize.Width;
                            }
                        }
                        // If moving right, subtract speed, check for tiles all the way to the left
                        // If there's any, move them all the way to the right
                        else {
                            positions[i][j].X -= speed[i];
                            if (positions[i][j].X <= -layerSize.Width) {
                                positions[i][j].X = layerSize.Width * (positions[i].Length - 1);
                            }
                        }
                    }
                }
            }
        }

        public void Draw(SpriteBatch batch) {
            for (int i = 0; i < num_layers; i++) {
                for (int j = 0; j < positions[i].Length; j++) {
                    Rectangle rectbg = new Rectangle(i * layerSize.Width, 0, layerSize.Width, layerSize.Height);
                    Rectangle destRect = new Rectangle((int)positions[i][j].X, (int)positions[i][j].Y, screenSize.Width, screenSize.Height);
                    batch.Draw(texture, destRect, rectbg, Color.White);
                }

            }
        }

    }
}
