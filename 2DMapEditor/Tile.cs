﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Mario_Clone {
    
    struct Tile {
        public static int textureSize;
        public static short maxID = 11;

        //tileType and Position are the ones that have to be stored in serialization
        short type;
        public short tileType {
            get {
                return type;
            }
            set {
                if (value > maxID) {
                    type = maxID;
                }
                else if (value < 0) {
                    type = 0;
                }
                else {
                    type = value;
                }
                changeBounds();
            }
        }
        public Vector2 VectorPosition {
            get {
                return Position.ToVector2();
            }
        }

        public Point Position {
            get;
            set;
        }

        //Point pos;
        public Rectangle Bounds;

        #region
        // Tile boundaries
        public static Rectangle Blank {
            get {
                return new Rectangle(0, 0, 0, 0);
            }
        }
        public static Rectangle leftGround {
            get {
                return new Rectangle(3 * textureSize, 5 * textureSize, textureSize, textureSize);
            }
        }
        public static Rectangle rightGround {
            get {
                return new Rectangle(4 * textureSize, 5 * textureSize, textureSize, textureSize);
            }
        }
        public static Rectangle Underground {
            get {
                return new Rectangle(2 * textureSize, 3 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle upRightRamp {
            get {
                return new Rectangle(5 * textureSize, 4 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle belowUpRamp {
            get {
                return new Rectangle(2 * textureSize, 5 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle belowDownRamp {
            get {
                return new Rectangle(5 * textureSize, 5 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle downRightRamp {
            get {
                return new Rectangle(2 * textureSize, 4 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle leftPlatformTop {
            get {
                return new Rectangle(0, 4 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle rightPlatformTop {
            get {
                return new Rectangle(1 * textureSize, 4 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle leftPlatformEdge {
            get {
                return new Rectangle(0 * textureSize, 5 * textureSize, textureSize, textureSize);
            }
        }

        public static Rectangle rightPlatformEdge {
            get {
                return new Rectangle(1 * textureSize, 5 * textureSize, textureSize, textureSize);   
            }
        }

        #endregion

        void changeBounds() {
            switch (type) {
                case 0:
                    Bounds = Blank;
                    break;
                case 1:
                    Bounds = leftGround;
                    break;
                case 2:
                    Bounds = rightGround;
                    break;
                case 3:
                    Bounds = Underground;
                    break;
                case 4:
                    Bounds = upRightRamp;
                    break;
                case 5:
                    Bounds = downRightRamp;
                    break;
                case 6:
                    Bounds = belowUpRamp;
                    break;
                case 7:
                    Bounds = belowDownRamp;
                    break;
                case 8:
                    Bounds = leftPlatformTop;
                    break;
                case 9:
                    Bounds = rightPlatformTop;
                    break;
                case 10:
                    Bounds = leftPlatformEdge;
                    break;
                case 11:
                    Bounds = rightPlatformEdge;
                    break;
                default:
                    Bounds = new Rectangle();
                    break;
            }
        }
        // Deprecated, use Tile(short, Point) instead
        public Tile(short id, Vector2 pos) : this() {
            type = id;
            Bounds = Blank;
            Position = pos.ToPoint();
            changeBounds();
        }

        public Tile(short id, Point position) : this() {
            Position = position;
            type = id;
            Bounds = Blank;
            changeBounds();
        }
    }
}

