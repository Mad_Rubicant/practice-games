﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Utilities;

using System;
using System.Collections.Generic;

using Shooter;
using Mario_Clone;
namespace _2DMapEditor {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MapEditor : Game {
        int mouseTolerance = 20;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D tileset;
        Map CurrentMap;

        Tile SelectedTile;
        MouseState prevMouseState;
        KeyboardState prevKeyboardState;
        Point mouseStart;
        // The camera is a vector located at the top left of the viewable area
        Vector2 Camera;
        public MapEditor()
            : base() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            // TODO: Add your initialization logic here
            Camera = new Vector2(0, 0);
            base.Initialize();
        }  

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Tile.textureSize = 32;
            tileset = Content.Load<Texture2D>("art/tilesheet");
            CurrentMap = new Map(tileset);
            SelectedTile = CurrentMap.Get(new Point(0, 0));

            for (int i = 0; i < GraphicsDevice.Viewport.TitleSafeArea.Width; i += Tile.textureSize) {
                CurrentMap.Add(new Tile((short)(1), new Vector2(i, GraphicsDevice.Viewport.TitleSafeArea.Height / 2)));
                for (int j = GraphicsDevice.Viewport.TitleSafeArea.Height / 2 + Tile.textureSize; j < GraphicsDevice.Viewport.TitleSafeArea.Height; j += Tile.textureSize) {
                    CurrentMap.Add(new Tile((short)((3) % Tile.maxID), new Point(i, j)));
                }
            }
            CurrentMap.ToFile("test.map");
            //CurrentMap.FromFile("test.map");
            //CurrentMap.DumpMapData();
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            MouseState mouse = Mouse.GetState();
            KeyboardState keyboard = Keyboard.GetState();

            if (mouse.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton != ButtonState.Pressed) {
                mouseStart = mouse.Position;
            }
            if (mouse.LeftButton == ButtonState.Released && prevMouseState.LeftButton == ButtonState.Pressed) {
                if (mouse.Position.ToVector2().Length() - mouseStart.ToVector2().Length() < mouseTolerance) {
                    // Finally do something with our mouse click
                    SelectedTile = CurrentMap.Get(mouseStart + Camera.ToPoint());
                }
            }

            if (keyboard.IsKeyDown(Keys.Right)) {
                Camera += new Vector2(1, 0);
            }
            if (keyboard.IsKeyDown(Keys.Down)) {
                Camera += new Vector2(0, 1);
            }
            if (keyboard.IsKeyDown(Keys.Up)) {
                Camera += new Vector2(0, -1);
            }
            if (keyboard.IsKeyDown(Keys.Left)) {
                Camera += new Vector2(-1, 0);
            }

            if (keyboard.IsKeyUp(Keys.Space) && prevKeyboardState.IsKeyDown(Keys.Space)) {
                if (SelectedTile.tileType == Tile.maxID)
                    SelectedTile.tileType = 0;
                else
                    SelectedTile.tileType++;
                CurrentMap.Update(SelectedTile);
            }

            if (((prevKeyboardState.IsKeyDown(Keys.LeftControl) || prevKeyboardState.IsKeyDown(Keys.RightControl)) && prevKeyboardState.IsKeyDown(Keys.S))) {
                CurrentMap.ToFile("test.map");
            }
            // TODO: Add your update logic here

            prevKeyboardState = keyboard;
            prevMouseState = mouse;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            CurrentMap.Draw(spriteBatch, Camera);
            // TODO: Add your drawing code here
            DrawGrid(spriteBatch, new Vector2(Tile.textureSize, Tile.textureSize), Camera);
            hilightTile(spriteBatch, SelectedTile);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void hilightTile(SpriteBatch batch, Tile tile) {
            Texture2D box = new Texture2D(GraphicsDevice, Tile.textureSize, Tile.textureSize);
            List<Color> colors = new List<Color>();
            int totalSize = box.Width * box.Height;
            for (int x = 0; x < totalSize; x++) {
                if (x < box.Width || x > totalSize - box.Width) {
                    colors.Add(Color.Red);
                }
                else if (x % box.Height == 0) {
                    colors.Add(Color.Red);
                }
                else if (x % box.Height == Tile.textureSize - 1) {
                    colors.Add(Color.Red);
                }
                else {
                    colors.Add(Color.Transparent);
                }
            }
            box.SetData<Color>(colors.ToArray());
            batch.Draw(box, new Rectangle(tile.Position.X - (int)Camera.X, tile.Position.Y - (int)Camera.Y, Tile.textureSize, Tile.textureSize), Color.White);
        }

        // Draw a grid on the screen. Widths is the x and y distance between each dash, start is the top left corner
        // of the grid
        private void DrawGrid(SpriteBatch batch, Vector2 Widths, Vector2 Start) {
            Start.X %= Tile.textureSize;
            Start.Y %= Tile.textureSize;
            Texture2D dash = new Texture2D(GraphicsDevice, 1, 5);
            Color[] dashColors = new Color[5];
            for (int i = 0; i < dashColors.Length; i++) {
                dashColors[i] = Color.Yellow;
            }
            dash.SetData<Color>(dashColors);

            for (int i = -(int)Start.X - Tile.textureSize; i < GraphicsDevice.Viewport.TitleSafeArea.Width; i += (int)Widths.X ) {
                for (int j = -(int)Start.Y - Tile.textureSize; j < GraphicsDevice.Viewport.TitleSafeArea.Height; j += (int)Widths.Y) {
                    batch.Draw(dash, new Rectangle(i - 1, j + (int)Widths.Y / 2 - 1, 1, 6), Color.White);
                    batch.Draw(dash, new Rectangle(i + (int)Widths.X / 2, j + (int)Widths.Y, 6, 1), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0f);
                }
            }
        }
    }
}
