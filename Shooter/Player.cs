﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    class Player {
        public Vector2 Position;
        public Vector2 Velocity;
        //public Texture2D Texture;
        public Animation PlayerAnimation;
        public Color color;
        public bool Active;
        public int Health;
        public int width {
            get {
                return PlayerAnimation.frameWidth;
            }
        }
        public int height {
            get {
                return PlayerAnimation.frameHeight;
            }
        }

        public Player() {

        }

        public void Initalize(Animation animation, Vector2 vector) {
            Position = vector;
            PlayerAnimation = animation;
            Active = true;
            Health = 100;
            color = Color.White;
        }

        public void Update(GameTime gameTime) {
            PlayerAnimation.Position = Position;
            PlayerAnimation.Update(gameTime);
        }

        public void Draw(SpriteBatch batch) {
            PlayerAnimation.Draw(batch);           
        }
    }
}
