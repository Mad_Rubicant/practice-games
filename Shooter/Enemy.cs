﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    class Enemy {
        public Animation enemyAnimation;
        public Vector2 Position;
        public bool Active;
        //public EnemyAI AI;
        public int HP;
        public int Damage;
        public int pointValue;
        public int Width {
            get {
                return enemyAnimation.frameWidth;
            }
        }
        public int Height {
            get {
                return enemyAnimation.frameHeight;
            }
        }
        internal float enemyAccel;
        internal float maxVelocity;
        internal Vector2 Velocity;
        internal Random random;

        public void Initialize(Animation animation, Vector2 position) {
            random = new Random();
            float velmult = -2.5f;
            Velocity.X = (float)random.NextDouble() * velmult;
            Velocity.X = MathHelper.Clamp(Velocity.X, velmult, -.5f);
            enemyAnimation = animation;
            Position = position;
            Active = true;
            HP = 10;
            Damage = 10;
            enemyAccel = -.5f;
            pointValue = 100;
            //AI = new EnemyAI();
        }

        public void Update(GameTime gameTime) {
            Position += Velocity;
            enemyAnimation.Position = Position;
            enemyAnimation.Update(gameTime);
            if (Position.X < -Width || HP <= 0) {
                Active = false;
            }
        }

        public void Draw(SpriteBatch batch) {
            enemyAnimation.Draw(batch);
        }
    }
}
