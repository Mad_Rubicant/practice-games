﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Shooter {
    class EnemyAI {
        
        static public void Update(ref Enemy enem) {
            Vector2 velUpdate = new Vector2(enem.enemyAccel, 0);
            enem.Velocity += velUpdate;
            enem.Velocity.X = MathHelper.Clamp(enem.Velocity.X, -enem.maxVelocity, enem.maxVelocity);
            enem.Velocity.Y = MathHelper.Clamp(enem.Velocity.Y, -enem.maxVelocity, enem.maxVelocity);
            Logging.Log(enem.Position.ToString());
        }
    }
}
