﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

using System;
using System.Collections;
using System.Collections.Generic;
namespace Shooter {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;
        Background Bglayer1, Bglayer2;
        float playerMaxVelocity = 8.0f;
        float playerAccel = 0.5f;
        KeyboardState lastKeyState, currentKeyState;
        MouseState lastMouseState, currentMouseState;

        Texture2D mainBackground;
        Rectangle rectBackground;
        float scale = 1f;

        Texture2D enemyTexture;
        List<Enemy> enemies;
        TimeSpan enemySpawnTime;
        TimeSpan previousSpawnTime;
        Random random;

        Texture2D projectileTexture;
        List<Projectile> projectiles;
        TimeSpan fireTime;
        TimeSpan previousFireTime;

        Texture2D explosionTexture;
        List<Animation> explosions;

        SoundEffect laserSound;
        SoundEffect explosionSound;
        Song bgMusic;

        int score;
        SpriteFont font;
        public Game1()
            : base() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            // TODO: Add your initialization logic here
            player = new Player();
            Bglayer1 = new Background();
            Bglayer2 = new Background();
            enemies = new List<Enemy>();
            previousSpawnTime = TimeSpan.Zero;
            enemySpawnTime = TimeSpan.FromSeconds(1.0f);
            projectiles = new List<Projectile>();
            fireTime = TimeSpan.FromSeconds(.15f);
            random = new Random();
            explosions = new List<Animation>();
            base.Initialize();
            score = 0;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Animation anim = new Animation();
            anim.Initialize(this.Content.Load<Texture2D>("shipAnimation"), Vector2.Zero, 115, 69, 8, 30, Color.White, .9f, true);
            player.Initalize(anim, new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2));
            Bglayer1.Initialize(Content, "bgLayer1", GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, -1);
            Bglayer2.Initialize(Content, "bgLayer2", GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Width, -2);

            projectileTexture = Content.Load<Texture2D>("laser");
            enemyTexture = Content.Load<Texture2D>("mineAnimation");
            mainBackground = Content.Load<Texture2D>("mainBackground");
            explosionTexture = Content.Load<Texture2D>("explosion");

            laserSound = Content.Load<SoundEffect>("Sound/laserFire");
            explosionSound = Content.Load<SoundEffect>("Sound/explosionSound");
            bgMusic = Content.Load<Song>("Sound/gameMusic");

            font = Content.Load<SpriteFont>("gameFont");    
            playMusic(bgMusic);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }




        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            player.Update(gameTime);

            lastKeyState = currentKeyState;
            lastMouseState = currentMouseState;
            currentKeyState = Keyboard.GetState();
            currentMouseState = Mouse.GetState();   
            UpdatePlayer(gameTime);
            UpdateEnemies(gameTime);
            UpdateCollisions();
            updateProjectiles();
            updateExplosions(gameTime);
            // TODO: Add your update logic here
            Bglayer1.Update(gameTime);
            Bglayer2.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            // TODO: Add your drawing code here
            spriteBatch.Draw(mainBackground, rectBackground, Color.White);
            Bglayer1.Draw(spriteBatch);
            Bglayer2.Draw(spriteBatch);
            foreach (Enemy enem in enemies) {
                enem.Draw(spriteBatch);
            }
            foreach (Projectile proj in projectiles) {
                proj.Draw(spriteBatch);
            }
            player.Draw(spriteBatch);
            foreach (Animation expl in explosions) {
                expl.Draw(spriteBatch);
            }


            spriteBatch.DrawString(font, "Score: " + score, new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X + 10, GraphicsDevice.Viewport.TitleSafeArea.Y), Color.White);
            spriteBatch.DrawString(font, "Health: " + player.Health, new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X + 10, GraphicsDevice.Viewport.TitleSafeArea.Y + 30), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
            
        }

        private void UpdatePlayer(GameTime time) {
            // If you're pressing in a direction, accelerate in that direction
            if (currentKeyState.IsKeyDown(Keys.Left)) {
                player.Velocity.X -= playerAccel;
            }
            if (currentKeyState.IsKeyDown(Keys.Right)) {
                player.Velocity.X += playerAccel;
            }
            if (currentKeyState.IsKeyDown(Keys.Down)) {
                player.Velocity.Y += playerAccel;
            }
            if (currentKeyState.IsKeyDown(Keys.Up)) {
                player.Velocity.Y -= playerAccel;
            }

            Vector2 mousePos = new Vector2(currentMouseState.X, currentMouseState.Y);

            if (currentMouseState.LeftButton == ButtonState.Pressed && currentKeyState.GetPressedKeys().Length == 0) {
                Vector2 posDelta = mousePos - player.Position;
                posDelta.Normalize();
                posDelta *= playerAccel;
                player.Velocity += posDelta;
            }

            // Air resistance based on velocity
            Vector2 airResist = new Vector2(player.Velocity.X / 20, player.Velocity.Y / 20);
            player.Velocity -= airResist;
            
            // No going past max velocity
            player.Velocity.X = MathHelper.Clamp(player.Velocity.X, -playerMaxVelocity, playerMaxVelocity);
            player.Velocity.Y = MathHelper.Clamp(player.Velocity.Y, -playerMaxVelocity, playerMaxVelocity);

            // Update position, and ensure you're not past the edge of the screen
            player.Position += player.Velocity;
            player.Position.X = MathHelper.Clamp(player.Position.X, 0, GraphicsDevice.Viewport.Width/* - player.width*/);
            player.Position.Y = MathHelper.Clamp(player.Position.Y, 0, GraphicsDevice.Viewport.Height /*- player.height*/);

            // If enough time has passed, spawn a projectile
            if (time.TotalGameTime - previousFireTime > fireTime) {
                previousFireTime = time.TotalGameTime;
                addProjectile(player.Position + new Vector2(player.width / 2, 0));
                laserSound.Play();
            }
            if (player.Health <= 0) {
                score = 0;
                player.Health = 100;
            }
        }

        private void addEnemy() {
            Animation enemAnimation = new Animation();
            enemAnimation.Initialize(enemyTexture, Vector2.Zero, 47, 61, 8, 30, Color.White, 1f, true);
            Vector2 position = new Vector2(GraphicsDevice.Viewport.Width + enemyTexture.Width / 2, random.Next(100, GraphicsDevice.Viewport.Height - 100));
            Enemy enemy = new Enemy();
            enemy.Initialize(enemAnimation, position);
            enemies.Add(enemy);
        }

        private void UpdateEnemies(GameTime time) {
            if (time.TotalGameTime - previousSpawnTime > enemySpawnTime) {
                previousSpawnTime = time.TotalGameTime;
                addEnemy();
            }
            for (int i = enemies.Count - 1; i > 0; i--) {
                enemies[i].Update(time);
                if (enemies[i].Active == false) {
                    score += enemies[i].pointValue;
                    addExplosion(enemies[i].Position);
                    Logging.Log("Destroyed enemy at" + enemies[i].Position.ToString());
                    Logging.Log("Explosion spawned at" + explosions[explosions.Count - 1].Position.ToString());
                    enemies.RemoveAt(i);
                    
                }
            }
        }

        private void UpdateCollisions() {
            Rectangle pRect, eRect;
            pRect = new Rectangle((int)player.Position.X, (int)player.Position.Y, player.width, player.height);
            Rectangle projRect;
            for (int i = 0; i < enemies.Count; i++) {
                eRect = new Rectangle((int)enemies[i].Position.X, (int)enemies[i].Position.Y, enemies[i].Width, enemies[i].Height);
                
                if (pRect.Intersects(eRect)) {
                    player.Health -= enemies[i].Damage;
                    enemies[i].HP = 0;

                    if (player.Health <= 0) {
                        player.Active = false;
                    }
                }
                for (int j = 0; j < projectiles.Count; j++) {
                    Projectile tmp = projectiles[j];
                    projRect = new Rectangle((int)tmp.Position.X, (int)tmp.Position.Y, tmp.Width, tmp.Height);
                    if (projRect.Intersects(eRect)) {
                        enemies[i].HP -= tmp.Damage;
                        projectiles[j].Active = false;
                    }
                }
            }
        }

        private void addProjectile(Vector2 Position) {
            Projectile proj = new Projectile();
            proj.Initialize(GraphicsDevice.Viewport, projectileTexture, Position);
            projectiles.Add(proj);
        }

        private void updateProjectiles() {
            for (int i = 0; i < projectiles.Count; i++) {
                projectiles[i].Update();
                if (projectiles[i].Active == false) {
                    projectiles.RemoveAt(i);
                }
            }
        }

        private void addExplosion(Vector2 Position) {
            Animation expl = new Animation();
            expl.Initialize(explosionTexture, Position, 134, 134, 12, 45, Color.White, 1.0f, false);
            explosions.Add(expl);
            explosionSound.Play();
        }

        private void updateExplosions(GameTime time) {
            for (int i = explosions.Count - 1; i >= 0; i--) {
                explosions[i].Update(time);
                if (explosions[i].Active == false) {
                    explosions.RemoveAt(i);
                }
            }
        }

        private void playMusic(Song song) {
            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = true;
        }
    }
}
