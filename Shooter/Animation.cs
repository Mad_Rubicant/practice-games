﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Shooter {
    class Animation {
        Texture2D spriteStrip;
        float scale;
        int elapsedTime;
        int frameTime;
        int frameCount;
        int currentFrame;
        Color color;
        Rectangle sourceRect = new Rectangle();
        Rectangle destRect = new Rectangle();
        public int frameWidth;
        public int frameHeight;
        public bool Active;
        public bool Looping;
        public Vector2 Position;

        public void Initialize(Texture2D texture, Vector2 position, int frame_width, int frame_height, int frame_count, int frame_time,
        Color col, float scale, bool Looping) {
            this.Position = position;
            this.frameWidth = frame_width;
            this.frameHeight = frame_height;
            this.frameCount = frame_count;
            this.frameTime = frame_time;
            this.color = col;
            this.scale = scale;
            this.Looping = Looping;
            this.spriteStrip = texture;

            elapsedTime = 0;
            currentFrame = 0;

            Active = true;
        }

        public void Update(GameTime gameTime) {
            if (Active == false) return;

            elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime > frameTime) {
                currentFrame++;

                if (currentFrame == frameCount) {
                    currentFrame = 0;
                    if (Looping == false)
                        Active = false;
                }
                elapsedTime = 0;
            }

            sourceRect = new Rectangle(currentFrame * frameWidth, 0, frameWidth, frameHeight);
            destRect = new Rectangle(((int)Position.X  - (int)(frameWidth * scale) / 2), (int)(Position.Y - (frameHeight * scale) / 2),
                (int)(frameWidth * scale), (int)(frameHeight * scale));
            Logging.Log(spriteStrip.Name + ", " + destRect.ToString());
        }
        public void Draw(SpriteBatch batch) {
            if (Active) {
                batch.Draw(spriteStrip, destRect, sourceRect, color);
                //Logging.Log(destRect.ToString());
                //Logging.Log(spriteStrip.Name);
            }
        }

    }
}
