﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Shooter {
    class Background {
        Texture2D texture;
        Vector2[] positions;
        int speed;
        int bgwidth;
        int bgheight;

        public void Initialize(ContentManager content, String texturePath, int screenwidth, int screenheight, int speed) {
            texture = content.Load<Texture2D>(texturePath);
            this.speed = speed;
            bgheight = screenheight;
            bgwidth = screenwidth;
            positions = new Vector2[screenwidth / texture.Width + 1];

            for (int i = 0; i < positions.Length; i++) {
                positions[i] = new Vector2(i * texture.Width, 0);
            }
        }

        public void Update(GameTime gameTime) {
            for (int i = 0; i < positions.Length; i++) {
                positions[i].X += speed;
                if (speed <= 0) {
                    if (positions[i].X <= -texture.Width) {
                        positions[i].X = texture.Width * (positions.Length - 1);
                    }
                }
                else {
                    if (positions[i].X >= texture.Width * (positions.Length - 1)) {
                        positions[i].X = -texture.Width;
                    }
                }
            }
        }

        public void Draw(SpriteBatch batch) {
            for (int i = 0; i < positions.Length; i++) {
                Rectangle rectbg = new Rectangle((int)positions[i].X, (int)positions[i].Y, bgwidth, bgheight);
                batch.Draw(texture, rectbg, Color.White);
            }
            
        }

    }
}
