﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Shooter {
    class Projectile {
        public Vector2 Position;
        public Texture2D Texture;
        public bool Active;
        public int Damage;
        Viewport Port;
        public int Width {
            get {
                return Texture.Width;
            }
        }
        public int Height {
            get {
                return Texture.Height;
            }
        }
        Vector2 Velocity;

        public void Initialize(Viewport port, Texture2D tex, Vector2 position) {
            Texture = tex;
            Port = port;
            Position = position;
            Active = true;
            Damage = 2;
            Velocity.X = 20f;
        }

        public void Update() {
            Position += Velocity;
            if (Position.X + Texture.Width / 2 > Port.Width) {

            }
        }
           
        public void Draw(SpriteBatch batch) {
            batch.Draw(Texture, Position, null, Color.White, 0f, new Vector2(Width / 2, Height / 2), 1f, SpriteEffects.None, 0f);
        }
    }
}
